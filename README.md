MYRESUME
========

This is my resume.
I am also doing this as a way to learn Git, Markdown, and Github.

The makefile included allows me to use pandoc to convert the Github-flavoured Markdown file into other formats. 

Special thanks to my friend Jason for pointing out the use of a Make file for Pandoc to HTML conversion.
